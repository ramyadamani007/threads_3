/******************
	INCLUDE
******************/

#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>

void *increment_even (void *arg);		/**** Function 1 Declaration ****/
void *increment_odd (void *arg1);		/**** Function 2 Declaration ****/

/*** Main ***/

int main()
{
	int a,b,c;
	pthread_t p,q,r;
	char m[] = "Incrementing even number.\n";
	char n[] = "Incrementing odd number.\n";
	
	a = pthread_create(&p, NULL, increment_even, (void *)m);
	
	if (a == 0)
	{
		printf("Thread creation successfull.\n");
		printf("Incrementing even number.\n");
	}
	
	b = pthread_create(&q, NULL, increment_odd, (void *)n);
	
	if (b == 0)
	{
		printf("Thread creation successfull.\n");
		printf("Incrementing odd number.\n");
	}
	
	if (a!=0 && b!=0)
	{
		printf("Thread creation failed.\n");
		exit (EXIT_FAILURE);
	}
	return 0;
}


void *increment_even (void *arg)	/**** Function 1 Initialization ****/
{
	int x;
	for (x=2; x<=11; x=x+2)
	{
		printf("Incremented even number is %d\n",x);
		printf("%s\n", (char *)arg);
	}
}


void *increment_odd (void *arg1)	/**** Function 2 Initialization ****/
{
	int y;
	for (y=1; y<=11; y=y+2)
	{
		printf("Incremented odd number is %d\n",y);
		printf("%s\n", (char *)arg1);
	}
}

		








		
		
	
