/******************
	INCLUDE
******************/

#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>

void *threadfun (void *arg);		/**** Function Declaration ****/

/*** Main ***/

int main()
{
	int a,b,c;
	pthread_t p,q,r;
	char m[] = "Learning Posix Thread.";
	char n[] = "The thread is a sequence of control with process.";
 	
	a = pthread_create(&p, NULL, threadfun, (void* )m);
	if (a == 0)
	{
		printf("Thread Creation Successfull.\n");
		printf("PID of thread 1 is %lu.\n",p);
		printf("PPID of Thread 1 is %d.\n",getppid());
	}
	
	b = pthread_create(&q, NULL, threadfun, (void* )n);
	if (b == 0)
	{
		printf("Another Thread Creation Successfull.\n");
		printf("PID of thread 2 is %lu.\n",q);
		printf("PPID of Thread 2 is %d.\n",getppid());
	}
	
	if (a!=0 && b!=0)
	{
		printf("Thread creation failed.\n");
		exit (EXIT_FAILURE);
	}
	return 0;
}

void *threadfun (void *arg)	
{
	printf("Thread function is running. \nAim was %s\n", (char *)arg);
	printf("It was great fun learning posix thread.\n");
	printf("Bye!\n");
}
	
	
	
	
	

	
